﻿namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

public class Preference
    :BaseEntity
{
    public string? Name { get; set; }

    public ICollection<Customer> Customers { get; set; } = new List<Customer>();
    public ICollection<PromoCode> PromoCodes { get; set; } = new List<PromoCode>();
}