﻿namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions;
public interface IDatabaseInitializer
{
    void Execute();
}
