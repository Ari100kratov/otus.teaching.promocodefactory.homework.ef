﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories.Contracts;
public interface IRoleRepository : IRepositoryBase<Role>
{
}
