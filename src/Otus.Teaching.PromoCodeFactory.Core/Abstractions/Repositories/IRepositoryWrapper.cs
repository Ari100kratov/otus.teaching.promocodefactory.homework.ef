﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories.Contracts;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
public interface IRepositoryWrapper
{
    Task Save();

    IEmployeeRepository Employee { get; }
    IRoleRepository Role { get; }
    ICustomerRepository Customer { get; }
    IPreferenceRepository Preference { get; }
    IPromoCodeRepository PromoCode { get; }
}
