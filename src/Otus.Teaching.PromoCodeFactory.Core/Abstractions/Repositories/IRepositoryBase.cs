﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System.Linq.Expressions;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
public interface IRepositoryBase<T> where T : BaseEntity
{
    IQueryable<T> FindAll();
    IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression);
    ValueTask<T?> Get(Guid id);

    void Add(T entity);
    Task AddRange(IEnumerable<T> entities);

    void Edit(T entity);
    void EditRange(IEnumerable<T> entities);

    void Delete(T entity);
    void DeleteRange(IEnumerable<T> entities);
}
