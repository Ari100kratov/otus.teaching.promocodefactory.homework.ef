﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess;
public class DatabaseInitializer : IDatabaseInitializer
{
    private readonly PromoCodeDbContext _context;
    public DatabaseInitializer(PromoCodeDbContext context) => _context = context;

    public void Execute()
    {
        _context.Database.Migrate();

        if (_context.Employee.Any())
            return;

        _context.Employee.AddRange(FakeDataFactory.Employees);
        _context.Customer.AddRange(FakeDataFactory.Customers);
        _context.SaveChanges();
    }
}
