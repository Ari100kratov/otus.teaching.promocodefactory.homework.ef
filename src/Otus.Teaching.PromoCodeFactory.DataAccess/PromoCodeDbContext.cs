﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Reflection;

namespace Otus.Teaching.PromoCodeFactory.DataAccess;
public class PromoCodeDbContext : DbContext
{
    public PromoCodeDbContext(DbContextOptions<PromoCodeDbContext> options)
            : base(options)
    { }

    public DbSet<Employee> Employee { get; set; }
    public DbSet<Role> Role { get; set; }
    public DbSet<Customer> Customer { get; set; }
    public DbSet<Preference> Preference { get; set; }
    public DbSet<PromoCode> PromoCodes { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        base.OnModelCreating(modelBuilder);
    }
}
