﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Configurations;
public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
{
    public void Configure(EntityTypeBuilder<Customer> builder)
    {
        builder.HasKey(x => x.Id);
        builder.Property(x => x.FirstName).HasMaxLength(50);
        builder.Property(x => x.LastName).HasMaxLength(50);
        builder.Ignore(x => x.FullName);
        builder.Property(x => x.Email).HasMaxLength(255);

        builder.HasMany(x => x.Preferences)
            .WithMany(x => x.Customers);

        builder.HasMany(x => x.PromoCodes)
            .WithMany(x => x.Customers);
    }
}
