﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Configurations;
public class PromoCodeConfiguration : IEntityTypeConfiguration<PromoCode>
{
    public void Configure(EntityTypeBuilder<PromoCode> builder)
    {
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Code).HasMaxLength(50);
        builder.Property(x => x.ServiceInfo).HasMaxLength(255);
        builder.Property(x => x.PartnerName).HasMaxLength(255);

        builder.HasOne(x => x.PartnerManager)
            .WithMany(x => x.PromoCodes);

        builder.HasOne(x => x.Preference)
            .WithMany(x => x.PromoCodes);
    }
}
