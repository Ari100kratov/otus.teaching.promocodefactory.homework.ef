﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations;

/// <inheritdoc />
public partial class Initial : Migration
{
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "Customer",
            columns: table => new
            {
                Id = table.Column<Guid>(type: "uuid", nullable: false),
                FirstName = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                LastName = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                Email = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true)
            },
            constraints: table =>
            {
                table.PrimaryKey("PK_Customer", x => x.Id);
            });

        migrationBuilder.CreateTable(
            name: "Preference",
            columns: table => new
            {
                Id = table.Column<Guid>(type: "uuid", nullable: false),
                Name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true)
            },
            constraints: table =>
            {
                table.PrimaryKey("PK_Preference", x => x.Id);
            });

        migrationBuilder.CreateTable(
            name: "Role",
            columns: table => new
            {
                Id = table.Column<Guid>(type: "uuid", nullable: false),
                Name = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                Description = table.Column<string>(type: "text", nullable: true)
            },
            constraints: table =>
            {
                table.PrimaryKey("PK_Role", x => x.Id);
            });

        migrationBuilder.CreateTable(
            name: "CustomerPreference",
            columns: table => new
            {
                CustomersId = table.Column<Guid>(type: "uuid", nullable: false),
                PreferencesId = table.Column<Guid>(type: "uuid", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey("PK_CustomerPreference", x => new { x.CustomersId, x.PreferencesId });
                table.ForeignKey(
                    name: "FK_CustomerPreference_Customer_CustomersId",
                    column: x => x.CustomersId,
                    principalTable: "Customer",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CustomerPreference_Preference_PreferencesId",
                    column: x => x.PreferencesId,
                    principalTable: "Preference",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
            });

        migrationBuilder.CreateTable(
            name: "Employee",
            columns: table => new
            {
                Id = table.Column<Guid>(type: "uuid", nullable: false),
                FirstName = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                LastName = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                Email = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                AppliedPromocodesCount = table.Column<int>(type: "integer", nullable: false),
                RoleId = table.Column<Guid>(type: "uuid", nullable: true)
            },
            constraints: table =>
            {
                table.PrimaryKey("PK_Employee", x => x.Id);
                table.ForeignKey(
                    name: "FK_Employee_Role_RoleId",
                    column: x => x.RoleId,
                    principalTable: "Role",
                    principalColumn: "Id");
            });

        migrationBuilder.CreateTable(
            name: "PromoCodes",
            columns: table => new
            {
                Id = table.Column<Guid>(type: "uuid", nullable: false),
                Code = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                ServiceInfo = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                BeginDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                EndDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                PartnerName = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                PartnerManagerId = table.Column<Guid>(type: "uuid", nullable: true),
                PreferenceId = table.Column<Guid>(type: "uuid", nullable: true)
            },
            constraints: table =>
            {
                table.PrimaryKey("PK_PromoCodes", x => x.Id);
                table.ForeignKey(
                    name: "FK_PromoCodes_Employee_PartnerManagerId",
                    column: x => x.PartnerManagerId,
                    principalTable: "Employee",
                    principalColumn: "Id");
                table.ForeignKey(
                    name: "FK_PromoCodes_Preference_PreferenceId",
                    column: x => x.PreferenceId,
                    principalTable: "Preference",
                    principalColumn: "Id");
            });

        migrationBuilder.CreateTable(
            name: "CustomerPromoCode",
            columns: table => new
            {
                CustomersId = table.Column<Guid>(type: "uuid", nullable: false),
                PromoCodesId = table.Column<Guid>(type: "uuid", nullable: false)
            },
            constraints: table =>
            {
                table.PrimaryKey("PK_CustomerPromoCode", x => new { x.CustomersId, x.PromoCodesId });
                table.ForeignKey(
                    name: "FK_CustomerPromoCode_Customer_CustomersId",
                    column: x => x.CustomersId,
                    principalTable: "Customer",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_CustomerPromoCode_PromoCodes_PromoCodesId",
                    column: x => x.PromoCodesId,
                    principalTable: "PromoCodes",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
            });

        migrationBuilder.CreateIndex(
            name: "IX_CustomerPreference_PreferencesId",
            table: "CustomerPreference",
            column: "PreferencesId");

        migrationBuilder.CreateIndex(
            name: "IX_CustomerPromoCode_PromoCodesId",
            table: "CustomerPromoCode",
            column: "PromoCodesId");

        migrationBuilder.CreateIndex(
            name: "IX_Employee_RoleId",
            table: "Employee",
            column: "RoleId");

        migrationBuilder.CreateIndex(
            name: "IX_PromoCodes_PartnerManagerId",
            table: "PromoCodes",
            column: "PartnerManagerId");

        migrationBuilder.CreateIndex(
            name: "IX_PromoCodes_PreferenceId",
            table: "PromoCodes",
            column: "PreferenceId");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable(
            name: "CustomerPreference");

        migrationBuilder.DropTable(
            name: "CustomerPromoCode");

        migrationBuilder.DropTable(
            name: "Customer");

        migrationBuilder.DropTable(
            name: "PromoCodes");

        migrationBuilder.DropTable(
            name: "Employee");

        migrationBuilder.DropTable(
            name: "Preference");

        migrationBuilder.DropTable(
            name: "Role");
    }
}
