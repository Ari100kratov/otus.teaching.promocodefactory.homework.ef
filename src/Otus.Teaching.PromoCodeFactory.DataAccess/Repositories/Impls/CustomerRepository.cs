﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories.Contracts;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Impls;
public class CustomerRepository : RepositoryBase<Customer>, ICustomerRepository
{
    public CustomerRepository(PromoCodeDbContext context) : base(context)
    {
    }
}
