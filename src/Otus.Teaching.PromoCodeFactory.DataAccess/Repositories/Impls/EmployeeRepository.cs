﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories.Contracts;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Impls;
public class EmployeeRepository : RepositoryBase<Employee>, IEmployeeRepository
{
    public EmployeeRepository(PromoCodeDbContext context) : base(context)
    {
    }
}
