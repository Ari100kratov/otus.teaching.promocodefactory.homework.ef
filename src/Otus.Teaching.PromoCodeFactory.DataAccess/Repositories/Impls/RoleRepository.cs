﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories.Contracts;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Impls;
public class RoleRepository : RepositoryBase<Role>, IRoleRepository
{
    public RoleRepository(PromoCodeDbContext context) : base(context)
    {
    }
}
