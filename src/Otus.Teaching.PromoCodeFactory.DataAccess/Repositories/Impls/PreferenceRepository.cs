﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories.Contracts;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Impls;
public class PreferenceRepository : RepositoryBase<Preference>, IPreferenceRepository
{
    public PreferenceRepository(PromoCodeDbContext context) : base(context)
    {
    }
}
