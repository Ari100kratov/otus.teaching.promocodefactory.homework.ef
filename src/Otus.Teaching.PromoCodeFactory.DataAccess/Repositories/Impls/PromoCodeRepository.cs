﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories.Contracts;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Impls;
public class PromoCodeRepository : RepositoryBase<PromoCode>, IPromoCodeRepository
{
    public PromoCodeRepository(PromoCodeDbContext context) : base(context)
    {
    }
}
