﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories.Contracts;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Impls;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
public class RepositoryWrapper : IRepositoryWrapper
{
    private readonly PromoCodeDbContext _context;
    public RepositoryWrapper(PromoCodeDbContext context) => _context = context;

    public Task Save() => _context.SaveChangesAsync();

    private IEmployeeRepository _employee;
    public IEmployeeRepository Employee => _employee ??= new EmployeeRepository(_context);

    private IRoleRepository _role;
    public IRoleRepository Role => _role ??= new RoleRepository(_context);

    private ICustomerRepository _customer;
    public ICustomerRepository Customer => _customer ??= new CustomerRepository(_context);

    private IPreferenceRepository _preference;
    public IPreferenceRepository Preference => _preference ??= new PreferenceRepository(_context);

    private IPromoCodeRepository _promoCode;
    public IPromoCodeRepository PromoCode => _promoCode ??= new PromoCodeRepository(_context);
}
