﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System.Linq.Expressions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : BaseEntity
{
    private protected readonly PromoCodeDbContext _context;
    public RepositoryBase(PromoCodeDbContext context)
    {
        _context = context;
    }

    public IQueryable<T> FindAll() => _context.Set<T>().AsNoTracking();
    public ValueTask<T?> Get(Guid id) => _context.Set<T>().FindAsync(id);
    public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression) => _context.Set<T>().Where(expression);

    public void Add(T entity) => _context.Set<T>().Add(entity);
    public Task AddRange(IEnumerable<T> entities) => _context.Set<T>().AddRangeAsync(entities);

    public void Edit(T entity) => _context.Set<T>().Update(entity);
    public void EditRange(IEnumerable<T> entities) => _context.Set<T>().UpdateRange(entities);

    public void Delete(T entity) => _context.Set<T>().Remove(entity);
    public void DeleteRange(IEnumerable<T> entities) => _context.Set<T>().RemoveRange(entities);
}
