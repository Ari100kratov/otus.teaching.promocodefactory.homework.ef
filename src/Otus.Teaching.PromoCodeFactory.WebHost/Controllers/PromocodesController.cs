﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Mapper;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

/// <summary>
/// Промокоды
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class PromoCodesController : ControllerBase
{
    private readonly IRepositoryWrapper _repositoryWrapper;
    public PromoCodesController(IRepositoryWrapper repositoryWrapper) => _repositoryWrapper = repositoryWrapper;

    /// <summary>
    /// Получить все промокоды
    /// </summary>
    [HttpGet]
    public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromoCodesAsync()
    {
        var promoCodes = await _repositoryWrapper.PromoCode
            .FindAll()
            .Select(x => x.ToShortReponse())
            .ToListAsync();

        return promoCodes;
    }

    /// <summary>
    /// Создать промокод и выдать его клиентам с указанным предпочтением
    /// </summary>
    [HttpPost]
    public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
    {
        var preference = _repositoryWrapper.Preference.FindByCondition(x => x.Name == request.Preference).FirstOrDefault();
        if (preference is null)
            return NotFound($"Preference with Name = {request.Preference} not found");

        var customerWithPreferences = await _repositoryWrapper.Customer
            .FindByCondition(x => x.Preferences.Contains(preference))
            .Include(x => x.Preferences)
            .Include(x => x.PromoCodes)
            .ToListAsync();

        if (!customerWithPreferences.Any())
            return NotFound($"Customers with preferences = {request.Preference} not found");

        // попробуем найти партнера в таблице Employee и связать с промокодом (если я правильно понял бизнес-правила)
        var partner = _repositoryWrapper.Employee.FindByCondition(x => x.LastName == request.PartnerName).FirstOrDefault();
        var promoCode = request.ToEntity(partner, preference);

        customerWithPreferences.ForEach(x => x.PromoCodes.Add(promoCode));

        // увеличиваем кол-во на +1. Возможно, требуется увеличивать на кол-во выданных промокодов КАЖДОМУ клиенту
        if (partner is not null)
            partner.AppliedPromocodesCount++;

        await _repositoryWrapper.Save();
        return Ok();
    }
}