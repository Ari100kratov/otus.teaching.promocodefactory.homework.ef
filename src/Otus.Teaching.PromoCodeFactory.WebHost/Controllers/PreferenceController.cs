﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Mapper;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

/// <summary>
/// Предпочтения
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class PreferenceController : ControllerBase
{
    private readonly IRepositoryWrapper _repositoryWrapper;
    public PreferenceController(IRepositoryWrapper repositoryWrapper) => _repositoryWrapper = repositoryWrapper;

    /// <summary>
    /// Возвращает список всех предпочтений
    /// </summary>
    [HttpGet]
    public async Task<IEnumerable<PreferenceReponse>> GetPreferenceAsync()
    {
        var preferences = await _repositoryWrapper.Preference
            .FindAll()
            .Select(x => x.ToResponse())
            .ToListAsync();

        return preferences;
    }
}