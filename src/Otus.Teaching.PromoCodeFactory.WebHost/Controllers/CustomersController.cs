﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mapper;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

/// <summary>
/// Клиенты
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class CustomersController : ControllerBase
{
    private readonly IRepositoryWrapper _repositoryWrapper;
    public CustomersController(IRepositoryWrapper repositoryWrapper) => _repositoryWrapper = repositoryWrapper;

    /// <summary>
    /// Возвращает список всех клиентов
    /// </summary>
    [HttpGet]
    public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
    {
        var customers = await _repositoryWrapper.Customer.FindAll()
            .Select(x => x.ToShortReponse())
            .ToListAsync();

        return Ok(customers);
    }

    /// <summary>
    /// Возвращает клиента с информацией о промокадах и предпочтениях
    /// </summary>
    /// <param name="id">Идентификатор клиента</param>
    [HttpGet("{id}")]
    public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
    {
        var customer = await _repositoryWrapper.Customer
            .FindByCondition(x => x.Id == id)
            .Include(x => x.PromoCodes)
            .Include(x => x.Preferences)
            .FirstOrDefaultAsync();

        return customer is null 
            ? NotFound() 
            : Ok(customer.ToResponse());
    }

    /// <summary>
    /// Добавить нового клиента с переданным списком предпочтений
    /// </summary>
    [HttpPost]
    public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
    {
        var preferences = new List<Preference>(request.PreferenceIds.Count);
        foreach (var preferenceId in request.PreferenceIds)
        {
            var preference = await _repositoryWrapper.Preference.Get(preferenceId);
            if (preference is null)
                return NotFound();

            preferences.Add(preference);
        }

        var customer = request.ToEntity(preferences);
        _repositoryWrapper.Customer.Add(customer);
        await _repositoryWrapper.Save();
        return Ok();
    }

    /// <summary>
    /// Изменить существующего клиента, включая его предпочтения
    /// </summary>
    /// <param name="id">Идентификатор клиента</param>
    [HttpPut("{id}")]
    public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
    {
        var customer = await _repositoryWrapper.Customer
            .FindByCondition(x => x.Id == id)
            .Include(x => x.Preferences)
            .FirstOrDefaultAsync();

        if (customer is null)
            return NotFound();

        var preferences = new List<Preference>(request.PreferenceIds.Count);
        foreach (var preferenceId in request.PreferenceIds)
        {
            var preference = await _repositoryWrapper.Preference.Get(preferenceId);
            if (preference is null)
                return NotFound();

            preferences.Add(preference);
        }

        customer.FirstName = request.FirstName;
        customer.LastName = request.LastName;
        customer.Email = request.Email;
        customer.Preferences = preferences;
        _repositoryWrapper.Customer.Edit(customer);
        await _repositoryWrapper.Save();
        return Ok();
    }

    /// <summary>
    /// Удалить клиента вместе с его промокодами
    /// </summary>
    /// <param name="id">Идентификатор клиента</param>
    [HttpDelete]
    public async Task<IActionResult> DeleteCustomer(Guid id)
    {
        var customer = await _repositoryWrapper.Customer
            .FindByCondition(x => x.Id == id)
            .Include(x => x.Preferences)
            .Include(x => x.PromoCodes)
            .FirstOrDefaultAsync();

        if (customer is null)
            return NotFound();

        _repositoryWrapper.PromoCode.DeleteRange(customer.PromoCodes);
        _repositoryWrapper.Customer.Delete(customer);
        await _repositoryWrapper.Save();
        return Ok();
    }
}