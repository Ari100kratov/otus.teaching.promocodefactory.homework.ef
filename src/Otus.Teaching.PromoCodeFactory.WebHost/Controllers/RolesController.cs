﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Mapper;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

/// <summary>
/// Роли сотрудников
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class RolesController : ControllerBase
{
    private readonly IRepositoryWrapper _repositoryWrapper;

    public RolesController(IRepositoryWrapper repositoryWrapper) => _repositoryWrapper = repositoryWrapper;

    /// <summary>
    /// Получить все доступные роли сотрудников
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<IEnumerable<RoleItemResponse>> GetRolesAsync()
    {
        var roles = await _repositoryWrapper.Role.FindAll().ToListAsync();
        return roles.Select(x => x.ToResponse());
    }
}