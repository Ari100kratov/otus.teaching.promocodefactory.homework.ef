﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models;

public class PreferenceReponse
{
    public Guid Id { get; set; }
    public string? Name { get; set; }
}
