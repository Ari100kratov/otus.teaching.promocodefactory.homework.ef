﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models;

public class CustomerResponse
{
    public Guid Id { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? Email { get; set; }

    public List<PreferenceReponse> Preferences { get; set; } = new List<PreferenceReponse>();
    public List<PromoCodeShortResponse> PromoCodes { get; set; } = new List<PromoCodeShortResponse>();
}