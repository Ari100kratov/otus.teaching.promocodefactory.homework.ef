﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapper;

public static class PreferenceProfile
{
    public static PreferenceReponse ToResponse(this Preference preference)
    {
        return new PreferenceReponse
        {
            Id = preference.Id,
            Name = preference.Name,
        };
    }
}
