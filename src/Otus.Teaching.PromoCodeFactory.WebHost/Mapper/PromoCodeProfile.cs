﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapper;

public static class PromoCodeProfile
{
    public static PromoCodeShortResponse ToShortReponse(this PromoCode promoCode)
    {
        return new PromoCodeShortResponse
        {
            Id = promoCode.Id,
            Code = promoCode.Code,
            ServiceInfo = promoCode.ServiceInfo,
            BeginDate = promoCode.BeginDate.ToString(),
            EndDate = promoCode.EndDate.ToString(),
            PartnerName = promoCode.PartnerName,
        };
    }

    public static PromoCode ToEntity(this GivePromoCodeRequest request, Employee? partner, Preference preference)
    {
        return new PromoCode
        {
            ServiceInfo = request.ServiceInfo,
            PartnerName = request.PartnerName,
            Code = request.PromoCode,
            BeginDate = request.BeginDate,
            EndDate = request.EndDate,
            PartnerManager = partner,
            Preference = preference
        };
    }
}
