﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapper;

public static class CustomerProfile
{
    public static CustomerShortResponse ToShortReponse(this Customer customer)
    {
        return new CustomerShortResponse
        {
            Id = customer.Id,
            FirstName = customer.FirstName,
            LastName = customer.LastName,
            Email = customer.Email,
        };
    }

    public static CustomerResponse ToResponse(this Customer customer)
    {
        return new CustomerResponse
        {
            Id = customer.Id,
            FirstName = customer.FirstName,
            LastName = customer.LastName,
            Email = customer.Email,
            PromoCodes = customer.PromoCodes.Select(x => x.ToShortReponse()).ToList(),
            Preferences = customer.Preferences.Select(x => x.ToResponse()).ToList()
        };
    }

    public static Customer ToEntity(this CreateOrEditCustomerRequest request, List<Preference> preferences)
    {
        return new Customer
        {
            FirstName = request.FirstName,
            LastName = request.LastName,
            Email = request.Email,
            Preferences = preferences
        };
    }
}
