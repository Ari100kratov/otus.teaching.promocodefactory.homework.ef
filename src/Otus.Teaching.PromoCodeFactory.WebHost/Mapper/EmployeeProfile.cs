﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapper;

public static class EmployeeProfile
{
    public static EmployeeShortResponse ToShortResponse(this Employee employee)
    {
        return new EmployeeShortResponse
        {
            Id = employee.Id,
            FullName = employee.FullName,
            Email = employee.Email,
        };
    }

    public static EmployeeResponse ToResponse(this Employee employee)
    {
        return new EmployeeResponse
        {
            Id = employee.Id,
            FullName = employee.FullName,
            Email = employee.Email,
            AppliedPromocodesCount = employee.AppliedPromocodesCount,
            Role = employee.Role?.ToResponse()
        };
    }

    public static RoleItemResponse ToResponse(this Role role)
    {
        return new RoleItemResponse
        {
            Id = role.Id,
            Name = role.Name,
            Description = role.Description,
        };
    }
}
